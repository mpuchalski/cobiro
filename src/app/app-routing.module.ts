import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerComponent } from './container/container.component';

const routes: Routes = [
  {
    path: 'team', component: ContainerComponent,
    loadChildren: () => import('./team/team.module').then(m => m.TeamModule)
  },
  { path: '**', redirectTo: '/team/our-team', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
