import { Component } from '@angular/core';

@Component({
  selector: 'cobiro-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cobiro';
}
