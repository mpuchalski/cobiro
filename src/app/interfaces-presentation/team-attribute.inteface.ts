import { TeamMemberCard } from './team-member-card.interface';

export interface TeamAttribute {
    title: string;
    memberCards: Array<TeamMemberCard>;
}
