import { TeamBlock } from './team-block.interface';
import { TeamImage } from './team-image.interface';

export interface TeamMemberCard {
    imageUrl: TeamImage;
    block: TeamBlock;
}
