import { TeamAttribute } from './team-attribute.inteface';

export interface Team {
    attributes: TeamAttribute;
}
