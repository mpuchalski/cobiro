export interface TeamBlock {
    title: string;
    description: string;
    link: string;
    text: string;
}