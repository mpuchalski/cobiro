import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OurTeamComponent } from './our-team/our-team.component';
import { OurTeamResolver } from './our-team/our-team.resolver';

const teamRoutes: Routes = [
  {
    path: 'our-team', component: OurTeamComponent, resolve: { team: OurTeamResolver }
  },
  { path: '**', redirectTo: '/', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forChild(teamRoutes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
