import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GetTeams } from '../interfaces-logic/get-teams.interface';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private httpClinet: HttpClient) { }

  /**
   * Getting team members
   */
  getTeams(): Observable<GetTeams> {
    return this.httpClinet.get<GetTeams>(`${environment.apiEndPoints.task}/index.json`);
  }
}
