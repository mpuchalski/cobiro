import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TeamService } from '../team.service';
import { map } from 'rxjs/operators';
import { get } from 'lodash-es';
import { Team } from 'src/app/interfaces-presentation/team.interface';

@Injectable()
export class OurTeamResolver implements Resolve<Observable<Array<Team>>> {

    constructor(private teamService: TeamService) { }

    resolve(): Observable<Observable<Array<Team>>> {
        return this.teamService.getTeams().pipe(map(m => {
            let team: Array<Team> = { ...get(m, 'data') };
            team = team ? Object.values(team)?.map(t => ({
                ...t,
                attributes: {
                    ...t.attributes,
                    memberCards: Object.values(t?.attributes?.memberCards ?? null)
                }
            })) : null;

            return of(team);
        }));
    }
}
