import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Team } from 'src/app/interfaces-presentation/team.interface';

@Component({
  selector: 'cobiro-our-team',
  templateUrl: './our-team.component.html',
  styleUrls: ['./our-team.component.scss']
})
export class OurTeamComponent implements OnInit {
  /**
   * Team members
   */
  $teamList: Observable<Array<Team>>;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.setObjectTeam();
  }

  /**
   * Load the team object from resolver
   */
  private setObjectTeam(): void {
    this.$teamList = this.activatedRoute.snapshot.data?.team;
  }
}
