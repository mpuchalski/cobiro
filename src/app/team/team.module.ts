import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurTeamComponent } from './our-team/our-team.component';
import { TeamRoutingModule } from './team-routing.module';
import { OurTeamResolver } from './our-team/our-team.resolver';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    OurTeamComponent
  ],
  imports: [
    CommonModule,
    TeamRoutingModule,
    SharedModule
  ],
  exports: [
    OurTeamComponent,
    TeamRoutingModule
  ],
  providers: [
    OurTeamResolver
  ]
})
export class TeamModule { }
