import { Component } from '@angular/core';

@Component({
  selector: 'cobiro-container',
  template: '<router-outlet></router-outlet>'
})
export class ContainerComponent {
  constructor() { }
}
