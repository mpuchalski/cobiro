import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamCardComponent } from './team-card/team-card.component';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [TeamCardComponent],
  imports: [
    CommonModule,
    LayoutModule
  ],
  exports: [TeamCardComponent]
})
export class SharedModule { }
