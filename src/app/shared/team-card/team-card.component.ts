import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { TeamMemberCard } from 'src/app/interfaces-presentation/team-member-card.interface';

@Component({
  selector: 'cobiro-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.scss']
})
export class TeamCardComponent implements OnDestroy, OnInit {
  /**
   * Input member
   */
  private _member: TeamMemberCard;
  /**
   * Link photo
   */
  backgroundImg: SafeStyle;

  get member(): TeamMemberCard {
    return this._member;
  }

  @Input() set member(val: TeamMemberCard) {
    this._member = val;
  }
  /**
   * Subscription breakpoint
   */
  breakpointSubscription: Subscription;

  constructor(
    private sanitizer: DomSanitizer,
    private breakpointObserver: BreakpointObserver) {
  }

  ngOnInit(): void {
    this.setImageUrl();
  }

  ngOnDestroy(): void {
    if (this.breakpointSubscription) {
      this.breakpointSubscription.unsubscribe();
    }
  }

  private setImageUrl(): void {
    this.breakpointSubscription = this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ]).subscribe((state: BreakpointState) => {
      if (state.breakpoints[Breakpoints.XSmall]) {
        this.backgroundImg = this.sanitizer.bypassSecurityTrustStyle(`url(${this._member?.imageUrl.w200})`);
      }
      if (state.breakpoints[Breakpoints.Small]) {
        this.backgroundImg = this.sanitizer.bypassSecurityTrustStyle(`url(${this._member?.imageUrl.w400})`);
      }
      if (state.breakpoints[Breakpoints.Medium]) {
        this.backgroundImg = this.sanitizer.bypassSecurityTrustStyle(`url(${this._member?.imageUrl.w1080})`);
      }
      if (state.breakpoints[Breakpoints.Large]) {
        this.backgroundImg = this.sanitizer.bypassSecurityTrustStyle(`url(${this._member?.imageUrl.w1920})`);
      }
      if (state.breakpoints[Breakpoints.XLarge]) {
        this.backgroundImg = this.sanitizer.bypassSecurityTrustStyle(`url(${this._member?.imageUrl.w2560})`);
      }
    });
  }
}
