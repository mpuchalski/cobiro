export interface GetTeams {
    data: Array<{
        type: string;
        id: string;
        attributes: {
            title: string;
            memberCards: {
                first: {
                    imageUrl: GetTeamsImageUrl;
                    block: GetTeamsBlock;
                },
                second: {
                    imageUrl: GetTeamsImageUrl;
                    block: GetTeamsBlock;
                },
                third: {
                    imageUrl: GetTeamsImageUrl;
                    block: GetTeamsBlock;
                }
            }
        }
    }>;
}

export interface GetTeamsImageUrl {
    w200: string;
    w400: string;
    w1080: string;
    w1920: string;
    w2560: string;
}

export interface GetTeamsBlock {
    title: string;
    description: string;
    link: string;
    text: string;
}
